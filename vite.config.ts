import { ConfigEnv, UserConfigExport } from 'vite';
import path from 'path';
import vue from '@vitejs/plugin-vue';
import vueJsx from '@vitejs/plugin-vue-jsx';
import { viteMockServe } from 'vite-plugin-mock';
import { buildConfig } from './build/config/buildConfig';
export default ({ command }: ConfigEnv): UserConfigExport => {
  return {
    base: '/',
    server: {
      port: 7874,
      proxy: {
        '/pweb/': 'http://118.144.87.46:38082',
      },
      hmr: {
        overlay: true,
      },
      open: true,
    },
    build: buildConfig,
    alias: {
      '/@': path.resolve(__dirname, '.', 'src'),
    },
    optimizeDeps: {
      include: [],
    },
    plugins: [
      vue(),
      vueJsx(),
      viteMockServe({
        ignore: /^_/,
        mockPath: 'mock',
        watchFiles: true, // 修改更新
        localEnabled:false //修改为command === 'serve',可根据运行环境判断是否使用mock拦截请求
      }),
    ],
  };
};
