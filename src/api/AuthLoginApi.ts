import { get, post } from '/@/utils/axios';

//登陆
export const GetBusinessCustomerRelation = (params: any) => {
  return post('/baseservice.GetBusinessCustomerRelation.do' , params)
}
//获取验证码
export const getImgToken = (params: any) => {
    return get('/getImgToken.do' , params)
}