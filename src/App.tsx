import { RouterLink, RouterView } from 'vue-router';
export default {
  render() {
    return (
      <section>
        <RouterView></RouterView>
        <bottom></bottom>
      </section>
    );
  },
};
