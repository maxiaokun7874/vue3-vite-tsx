import axios from 'axios';
import { baseURL } from '/@/config';
import {AxiosRequestConfig} from 'axios'
const AxiosConfig: AxiosRequestConfig  ={
  baseURL: baseURL,
  withCredentials: true,
  timeout: 60000,
  headers: {
    'content-type': 'application/json;charset=UTF-8',
  },
}
const instance = axios.create(AxiosConfig)

// 添加一个请求拦截器
instance.interceptors.request.use(function (config) {
  // Do something before request is sent
  return config;
}, function (error) {
  // Do something with request error
  return Promise.reject(error);
});

// 添加一个响应拦截器
instance.interceptors.response.use(function (response) {
  // Do something with response data
  return Promise.resolve(response.data);
}, function (error) {
  // Do something with response error
  return Promise.reject(error);
});
export default instance;
